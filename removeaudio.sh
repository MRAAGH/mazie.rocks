#!/bin/bash

for i in $(ls img/*\.webm); do
    echo silencing $i;
    ffmpeg -i "$i" -c copy -an "${i/\.webm/-silent.webm}"
done
