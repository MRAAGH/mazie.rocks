#!/usr/bin/python3

import yaml
import re
import os
import sys
import datetime

# regex for splitting file by ---
r = re.compile('\\n---\\n')


def formatdate(date):
    day = date.day
    ext = (
            'th' if day==11 or day==12 or day==13 else
            'st' if day%10==1 else
            'nd' if day%10==2 else
            'rd' if day%10==3 else
            'th')
    return date.strftime('%e{0} of %B %Y').format(ext)


def make_grid(elements, prefix):
    grid = '<nav class = "grid">\n'
    for el in elements:
        gridel = '  <div class="card"><a href="{linkurl}" title="{title}">\n'
        if 'date' in el:
            gridel += '    <div class="date">{date}</div>\n'.format(
                    date = el['date'].strftime('%b %d, %Y'))
        gridel += '    <img alt="{title} card" class="{cardclasses}" width="600" height="400" src="{cardurl}"><br />\n    '
        if 'icon' in el:
            gridel += '<img alt="" class="icon" src="{icon}"> '.format(
                    icon = el['icon'])
        gridel += '{title}</a>\n  </div>\n'
        grid += gridel.format(
                linkurl = prefix+el['filename'],
                title = el['title'],
                cardclasses = 'cardimg craziecardimg' if el['title'] == 'Crazie Mazie' else 'cardimg',
                cardurl = prefix+'../'+el['card'])
    grid += '</nav>\n'
    return grid


# hardcoded categories
catdict = {
        'Games': {
            'filename': 'games.html',
            'icon': 'img/icons/aagrinder.png',
            'card': 'img/games-card.jpg',
            'articles': []
            },
        'Coding': {
            'filename': 'coding.html',
            'icon': 'img/icons/coding.png',
            'card': 'img/hexmaze-card.png',
            'articles': []
            },
        'Modding': {
            'filename': 'modding.html' ,
            'icon': 'img/icons/screwdriver.png',
            'card': 'img/modding-card.jpg',
            'articles': []
            },
        '3D printing': {
            'filename': '3dprinting.html' ,
            'icon': 'img/icons/printer.png',
            'card': 'img/3dprinting-card.jpg',
            'articles': []
            },
        'Pony art': {
            'filename': 'ponies.html',
            'icon': 'img/icons/mazestare.png',
            'card': 'img/ponyart-card.jpg',
            'articles': []
            },
        'Weird stuff': {
            'filename': 'weird.html',
            'icon': 'img/icons/video.png',
            'card': 'img/cube-card.jpg',
            'articles': []
            },
        'About': {
            'filename': 'about.html',
            'icon': 'img/icons/mazie.png',
            'card': 'img/stick_mazie_question-card.png',
            'articles': []
            },
        }


if not os.path.exists('a'):
    os.makedirs('a')

# remove all files in that dir
for filename in os.listdir('a'):
    os.remove('a/'+filename)

# get the templates
with open('templates/template.html', 'r') as f:
    template_html = f.read()


article_fullfilenames = os.listdir('articles')
article_fullfilenames.sort()
for fullfilename in article_fullfilenames[::-1]:
    with open('articles/'+fullfilename, 'r') as f:
        filecontent = f.read()
    # the filename will be only the first part of the original filename
    filename = fullfilename.split('-', 1)[1]
    # source file has yml and html
    ymlstring, htmlstring = r.split(filecontent, 1)
    props = yaml.safe_load(ymlstring)
    props['filename'] = filename
    props['date'] = props['date']
    cat = props['category']
    if cat in catdict:
        catdict[cat]['articles'].append(props)
        # this is used later for generating category pages
    else:
        sys.stderr.write('No such category: '+cat+' ('+filename+')\n')
    # put all the html pieces together
    full_html_article = template_html.format(
            cssprefix = '../',
            mainhtml = htmlstring,
            title = props['title']+' - Mazie Rocks!',
            relpath = '../',
            footer = '<p>Date of last update: '
            + formatdate(props['date']) + '</p>',
    )
    with open('a/'+filename, 'w') as f:
        f.write(full_html_article)

if not os.path.exists('c'):
    os.makedirs('c')

# now generate the category pages
for catname in catdict:
    cat = catdict[catname]
    # prepare grid of articles
    grid = make_grid(cat['articles'], '../a/')
    # add title
    grid = '<h1>{0}</h1>\n'.format(catname) + grid
    # put all the html pieces together
    full_html_cat = template_html.format(
            cssprefix = '../',
            mainhtml = grid,
            title = catname+' - Mazie Rocks!',
            relpath = '../',
            footer = '',
            )
    with open('c/'+cat['filename'], 'w') as f:
        f.write(full_html_cat)

# finally, generate the home page

# OLD HOME PAGE: category list
# turn category dict into a list
catlist = [{
            'title': title,
            'filename': cat['filename'],
            'card': cat['card'],
            'icon': cat['icon']
        } for (title, cat) in catdict.items()][:-1]
# prepare grid of categories
catgrid = make_grid(catlist, 'c/')

biglist = []
for catname in catdict:
    cat = catdict[catname]
    biglist += cat['articles']
# sort big list by date
biglist = sorted(biglist, key=lambda el: el['date'], reverse=True)
filteredbiglist = list(filter(lambda el: el['category'] != 'About', biglist))
recentlist = filteredbiglist[:6]
recentgrid = make_grid(recentlist, 'a/')
grid = ('<h1>Hi! I\'m Maze! Here are some things I made!</h1>\n'
        + catgrid
        + '<h1>Recently updated:</h1>\n'
        + recentgrid)

# put all the html pieces together
full_html_home = template_html.format(
        cssprefix = '',
        mainhtml = grid,
        title = 'Mazie Rocks!',
        relpath = '',
        footer = '',
        )
with open('index.html', 'w') as f:
    f.write(full_html_home)


