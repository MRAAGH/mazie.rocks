#!/bin/bash

for i in $(ls img/*-big.*); do
    if test -f ${i/-big./-card.}; then
        # it already exists
        echo skipping $i;
    else
        echo converting $i;
        convert $i -strip -resize x400 -resize 600x\< -gravity center -extent 600x400 ${i/-big./-card.}
    fi
done
